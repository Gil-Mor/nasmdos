; ex1c. read one char from keyboard. if the char is lower-case then 
;       capsulate it, if the char is upper-case 'lower' it. 
;       then print the char to the screen.


ORG 0X100

section .data

	lowers_start: db 'a'       ; every char that is greater or equal to 'a' 
							   ; is lower-case

	convert: db 20h            ; differnce between lower and upper-case chars
							   ; add to lower, subtract to capsulate.

section .bss

   char: resb 1                ; reserve one byte for the input char.


section .text
	
	MOV AH, 01H                   ; to call the 'read input from keyboard' 
				                  ; function we need to insert the DOS 
				                  ; function number to AH in this case 01.

	INT 21H                       ; call DOS to call the function

	MOV [char], AL                ; the char was saved in AL.
                                  ; move it to reserved address.

    MOV AL, [lowers_start]        ; AL <- 'a'.  to compare input case.
    MOV BL, [convert]             ; BL <- 20H. to convert case.


    CMP [char], AL         ; compare input to 'a' ascii char. if the input
    					   ; is greater or equal then it's lower-case so jump
    					   ; to capsulate and then display. if not, 
    					   ; it's upper-case, fall through to lower case and
    					   ; then display.

    JGE CAPSULATE_LOWER    

    

    	         ; default fall-through if jmp condition doesn't happen
    LOWER_UPPER:

    	ADD [char], BL         ; [char] <- [char] + 20H
    	JMP DISPLAY            ; dont go to next condition. jump to display.


    CAPSULATE_LOWER:

    	SUB [char], BL        ; [char] <- [char] - 20H


    DISPLAY:                  ; display the char on the screen (stdout)

    	MOV DL, [char]        ; move char to DL, the output is read from DL.
    	MOV AH, 02H           ; enter 'display byte on screen' function 
    						  ; number to AH.

    	INT 21H               ; call DOS, DOS calls the function.


  END: MOV AX, 4C00H          ; exit - control to DOS
       INT 21H                ; call DOS