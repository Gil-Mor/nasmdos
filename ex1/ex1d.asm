; ex1d. read integer from 1 to 4 from the keyboard and display the name 
;       of the first month in that quarter of the year.

ORG 0X100

section .data

                     ; dont want so many conditions..
                     ; i'll scan the string and when i'll find number (char)
                     ; that equals to the input ill print from there until
                     ; the next 0. 

  months: db '1Janury',0, '2Fabruary',0,   '3July',0,   '4September',0


section .bss 

   input: resb 1          ; reserve one byte for the input char.


section .text

	LEA DI, [months]        ; using dI instead of SI for SCASB

	GET_INPUT:              ; get user input

      MOV AH, 01H         ; to call the 'read input from keyboard' function 
				                  ; we need to insert the DOS function number to AH
				                  ; in this case 01.

	    INT 21H             ; call DOS to call the function



  SEARCH:                 ; search for the input number in the string.

    	SCASB               ; compare AL (already has input char) to char byte
                          ; from months string
   
      JZ PRINT            ; jump to PRINT when found match.
      JMP SEARCH          ; continue search
  

  PRINT:                  ; display the month on the screen (stdout).
                          ; DI now points to the first char in the wanted month
      
      
    	MOV DL, [DI]        ; move char to DL, the output is read from DL.

      CMP BYTE DL, 0      ; if DL = 0 it's the end of the string.
      JZ END

    	MOV AH, 02H      ; enter 'display byte on screen' function number to AH.
    						        

    	INT 21H             ; call DOS, DOS calls the function.

      INC DI              ; go to next char in the string.
      

      JMP PRINT           ; loop print.


  END: MOV AX, 4C00H      ; exit - control to DOS
       INT 21H
