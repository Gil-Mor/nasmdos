; part a. calcuale and store the HEX values of 1F+2A,  2A-1F, 
; and their avarage (1F+2A)/2,  in logical addresses starting from 0x0200

ORG 0x100

section .data   

    x: dw 0x1F      ; x is pointer to the value 1F hex
    y: dw 0x2A      ; y is pointer to the value 2A hex

section .text

    mov SI, 0x0200  ; set SI to 0200 logical address DS:0200

                    ; inserting x to AX. using only one buffer for calculations.

    mov AX, [x]     ; AX <- x
                    
    add AX, [y]     ; AX <- AX + [y]   calculating x+y.

    mov [SI], AX    ; [SI] <- AX. put the sum in the logical adress 0200 
                    ; in the data segment

    add SI, 2       ; advance SI two bytes 2 give it the adress 0202


    sub AX, [x]      ; calculate y-x. AX contains x+y so subtract x twice 
    sub AX, [x]      ; from CX gives y-x.

    mov [SI], AX    ; [SI] <- AX = (y-x).
    add SI, 2

                     
    add AX, [x]     ; calculate (x+y)/2. their avarage.
    add AX, [x]     ; add x twice to AX (now AX = x-y)  make AX = x+y again

    SHR AX, 1       ; shift AX 1 bit left = (AX/2)


    HLT



