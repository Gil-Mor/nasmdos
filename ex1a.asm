; ex1a. capsulate the string 'hello' to the string 'HELLO'

ORG 0X100

section .data

  hello: db 'hello', 0      ; hello <- pointer to first word (2-bytes) 
							; in the String 
							; the value 0 represents the end of the string.

  len EQU $-hello           ; current address (end of string) - address
							; of the first char in the string
							; = length of string. will be used for loops

  to_caps: db 20h           ; subtract 20h from lower case ascii char
							; to capsulate it.
			 

section .bss

  CAPS: resb 6            ; reserve 6 bytes in the data segment.
						  ; 5 bytes for the ascii chars of the string
						  ; 'hello' and 1 byte for the terminating 0


section .text

  
  LEA SI, [hello]       ; load the effective address of hello pointer
						; to SI pointer. make SI point to the string
						; pointed by hello pointer. (same as mov SI, hello)

  LEA DI, [CAPS]        ; same as above but with DI and CAPS
	
  MOV CX, len           ; CX = length of string.


  CAPSULATE:            ; capsulate the string.

	LODSB               ; AL <- [SI] by convention, move byte from [SI] to AL
						; SI <- SI+1. forward SI to next byte in string

	SUB AL, [to_caps]
	  

	STOSB                ; [DI] <- AL. store the byte in AL in the array
						 ; DI <- DI+1. forward DI to next byte in dest array.


	LOOP CAPSULATE       ; CX <- CX-1. loop until CX = 0
		
	STOSB                ; store the '0' - \null. end of string.
 

  END: MOV AX, 4C00H      ; exit - control to DOS
	   INT 21H

