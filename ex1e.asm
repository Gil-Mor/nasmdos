; ex1e. open a file in the local nasm dir and write to it three lines of
;       animals drawings in ascii. 

ORG 0X100

section .data


  cr EQU 0x0d                ; cr=carriage return
  lf EQU 0x0a                ; lf=line feed
                             ; these would write newline after every line

                             ; strings to write to the file and their
                             ; numbers of bytes.

  quala: db '@(*0*)@',cr, lf
  quala_bytes EQU $-quala

  cat: db '=^..^=',   cr, lf
  cat_bytes EQU $-cat

  fish: db '><((((>', cr, lf
  fish_bytes EQU $-fish


                        ; when specifying full path 'C:\dos\c\bin\targil.txt'
                        ; the create file function returns code error 3 path
                        ; not found. when opening like this there's error
                        ; code 5 'access denied' but it writes to the file...

  file_name: db 'targil.txt', 00

  filehandle: dw 0                        ; for filehandler

section .text

  CREATE:                           ; create file  
	    MOV DX, file_name     
      MOV CX, 0000                  ; no special attributes
      MOV AH, 3CH                   ; DOS function number to create a file

      INT 21h                       ; call DOS to execute.
      MOV [filehandle], AX          ; save file handler in reserved address
  

  WRITE:                            ; write to file
      MOV AH, 40H                   ; call 'write to file function'
      MOV CX, quala_bytes           ; number of bytes to write in quala line
      MOV DX, quala                 ; pointer to the string to DX
      INT 21h                       ; call DOS to execute.

      MOV AH, 40H                   ; same for cat and fish. 
      MOV CX, cat_bytes
      MOV DX, cat
      INT 21h

      MOV AH, 40H
      MOV CX, fish_bytes
      MOV DX, fish
      INT 21h


   CLOSE:                           ; close the file.
      MOV BX, [filehandle]
      MOV AH, 3EH
      INT 21H


  END: MOV AX, 4C00H                ; control to DOS
       INT 21H
