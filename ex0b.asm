; part b. store 10 ascii chars, starting from 'H' and backwards in an array

ORG 0x100

section .data

    array: times 11 db 00     ; array <- pointer to the first byte from 11 bytes
                              ; initialized with a value 0. 11 bytes because  
                              ; need last byte for null since using String.
                              


    chars: db 'HGFEDCBA@?',00   ; chars <- pointer to first byte in the String 
                                ; (the H ascii char). the value 0 represents
                                ; the end of the string.

section .text
                        ; I choose to use String over numerical ascii

    mov SI, chars       ; SI <- pointer to chars (source)
    mov DI, array       ; DI <- pointer to array (destination)

                        ; we'll loop until the end of the string (0).
                        ; no need to count to 10..

L1:     LODSB           ; AL <- [SI] by convention, move byte from [SI] to AL
                        ; SI <- SI+1. forward SI to next byte in string


        CMP AL,0        ; compare AL to 0 (end of string)
        JZ end          ; if AL == 0 jump to end of program.

        STOSB           ; [DI] <- AL. store the byte in AL in the array
                        ; DI <- DI+1. forward DI to next byte in dest array.

        JMP L1          ; jmp to L1. loop until end of string.
        
end: STOSB              ; store \null. end of array.

     HLT 
