; ex1b. capsulate the string 'hello' to the string 'HELLO' using the stack.

ORG 0X100

section .data

    cr EQU 0x0d                ; cr=carriage return
    lf EQU 0x0a                ; lf=line feed

  hello: dW 'hello',lf,cr      ; hello <- pointer to first byte in the String 
                               ; the value 0 represents the end of the string.

  len EQU $-hello              ; current address (end of string) - address
                               ; of the first char in the string
                               ; = length of string. will be used for loops

  to_caps: db 20h              ; subtract 20h from lower case ascii char
                               ; to capsulate it.


section .bss

  CAPS: resW 6            ; reserve 6 bytes in the data segment.
                          ; 5 bytes for the ascii chars of the string
                          ; 'hello' and 1 byte for the terminating 0

section .text

  LEA SI, [hello]        ; load the effective address of hello pointer
                         ; to SI pointer. make SI point to the string
                         ; pointed by hello pointer. (same as mov SI, hello)

  LEA DI, [CAPS+len]     ; we'll write to CAPS backwards so we need to
                         ; start from the end.

  MOV CX, len            ; CX <- length of source string
                         ; loop by length of string. 


  PUSH_STRING: 

      LODSW                      ; load two bytes to AX
                                 ; AH <- [SI]
                                 ; AL <- [SI+1]

      SUB AL, [to_caps]          ; Capitalize char in AL
      SUB AH, [to_caps]          ; Capitalize char in AX 

        
      PUSH AX                    ; push the WORD  from AX to the stack
                                 ; [SP] <- AX,  SP - 2.

      LOOP PUSH_STRING           ; jmp to L1. loop until end of string.
        


    MOV CX, len                  ; pop from stack by string length

    POP_STRING: 

        POP WORD [DI]            ; [DI] <- [SP],  [DI+1] <-[SP+1], SP + 1
        SUB DI, 2                ; DI <- DI-2

        LOOP POP_STRING




  STOSW                          ; store the '00' - \null. end of string.


  END: MOV AX, 4C00H             ; exit - control to DOS 
       INT 21H

