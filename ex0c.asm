; part c. converting the numbers 1 2 3 4 5 from their ascii representation 
; to their HEX values.

ORG 0x100

section .data

                            ; array <-pointer to the first byte from 5 bytes
                            ; initialized with ascii chars '12345'.
                                        
    array: dw 0x31, 0x32, 0x33, 0x34, 0x35,    


    to_number: dw 0x30   ; the difference between values represting the numbers
                         ; 0-9 in ascii to there numerical value for example
                         ; '1' in ascii in represented as 31H so 31H - 30H = 1.


section .text

    mov SI, array               ; SI pointer to first WORD in the array
    mov CX, 5                   ; counter for loop. 

    mov AX, [to_number]         ; insert [to_number] to AX buffer.

L1:   

        sub [SI], AX            ; [SI] <- [SI] - AX. subtract 30 HEX from the 
                                ; ascii value make it the number in HEX.

        add SI, 2               ; forward SI pointer to next WORD in the array
        
        
        LOOP L1                 ; CMP CX to 0 and CX--. loop 5 times.

   
     HLT 



                              

