; ex2d. open the file created in ex1e, containing three lines of ascii drawing, 
;        overwrite the cat line with another quala line and close the file.

ORG 0X100


; --------------------- equates, definitions --------------------------------

      exit_command EQU 0x4c

      cr EQU 0x0D                    ; cr=carriage return
      lf EQU 0x0A                    ; lf=line feed

      
      %define dos_caller AH
      %define DOS int 0x21


; ------------------------------ macros --------------------------------------

      %macro open_write 3                ; the macro gets three parameters, filename to write to,  
                                         ; string to write to the file, and the string length

       ;----- open the file for writing
          xor AX, AX                     ; clean AX not to pass grabage arguments to functions
          mov dos_caller, 0x3D           ; open file function
          mov AL, 1                      ; open file for writing 
          mov DX, %1                     ; DX <- first parameter = filename
          DOS
          mov [filehandle], AX           ; mov returned file handler from AX to reserved place




      ;----- set offset to end of first line
           xor AX, AX                    ; clean AX not to pass grabage arguments to functions
           mov dos_caller, 0x42          ; set offset for over writing 

           mov CX, 0000                  ; CX <- higher part of offset
           mov DX, %3                    ; DX <- lower part of offser = third parameter = quala_bytes = 
                                         ; length of the first line in the file.
           mov BX, [filehandle]
           DOS                           ; call DOS to execute.
          
       
             
       ;----- overwrite second line
           xor AX, AX                    ; clean AX not to pass grabage arguments to functions
           mov dos_caller, 0x40          ; write to file function
           mov BX, [filehandle]
           mov CX, %3                    ; number of bytes to write to the file
           mov DX, %2                    ; DX <- string to write to the file.
           DOS

      %endmacro


      %macro close_file 1               ; gets one paramete - a file handler of the file to close.

           xor AX, AX                   ; clean AX not to pass grabage arguments to functions
           mov dos_caller, 0x3E         ; close file function
           mov BX, [%1]                 ; BX <- [first parameter = filehandler]
           DOS
           

      %endmacro

; -------------------------------- data --------------------------------------
section .data
                   
                                  ; quala ascii painting string and it's length.

  quala: db '@(*0*)@',cr,lf       ; first write line feed going to next line, then go to the start of that line, 
                                  ; then overwrite the line with quala string.
  quala_bytes EQU $-quala

  file_name: db 'c:\bin\targil.txt', 00   ; file path

  filehandle: dw 0                        ; for filehandler


; -------------------------------- PROGRAM ------------------------------------
section .text

    open_write file_name, quala, quala_bytes

    close_file (filehandle)             ; filehandle was set in the 'open_write' macro.


  END: MOV dos_caller, exit_command     ; exit - control to DOS
       DOS
