; ex2a. function that sums three hex numbers and saves the sum in the accumulator AX.

ORG 0X100


; --------------------- equates, definitions --------------------------------

    exit_command EQU 0x4c00  

    a EQU 0x4                ; define constants to sum
    b EQU 0x8     
    c EQU 0xF

    %define accumulator AX   ; using more meaningful names for buffers.
    %define dos_caller AX    ; AX has two rules.
    %define DOS int 0x21
   

;--------------------- macros ------------------------------------------------

%macro sum 3                 ; macro sum gets 3 hex values, and sums them
                             ; into the accumulator - AX.
                             ; AX suppose to be empty before calling the macro.
  
    add accumulator, %1      ; AX += first parameter (a=4)
    add accumulator, %2      ; AX += first parameter (b=8)
    add accumulator, %3      ; AX += first parameter (c=F)

%endmacro                    ; AX = 4+8+F = 0x1B


;--------------------- PROGRAM ------------------------------------------------
section .text
    
    xor ax, ax             ; empty AX before summing to it.
    sum a,b,c              ; call 'sum' macro with a, b, c as parameters.


END: mov dos_caller, exit_command    ; AX <- 0x4c00 = exit code for dos.
     DOS                             ; exit program.


