; ex2c. function that gets one number as parameter and sets the carry flag
;       to 1 if that number is odd, and clears the carry flag (to 0) if
;       the number is even.

ORG 0X100

; --------------------- equates, definitions --------------------------------

    exit_command EQU 0x4c00

    %define accumulator AX   ; using more meaningful names for buffers.
    %define dos_caller AX
    %define DOS int 0x21
    
;--------------------- macros ------------------------------------------------

    %macro divide_by_two 1   ; the macro gets one number, moves it to ax
                             ; and divides it by 2. if the parameter was
                             ; odd the carry flag will be set to 1.
                             ; if the parameter is even the carry flag will
                             ; be cleared (0)

       mov accumulator, %1   ; ax <- parameter 
       shr accumulator, 1    ; ax/2

    %endmacro

;----------------- PROGRAM ---------------------------------------------------
section .text
    
    divide_by_two(0x8)
    divide_by_two(0x5)


END: mov dos_caller, exit_command    ; AX <- 0x4c00 = exit code for dos.
     DOS                             ; exit program.


