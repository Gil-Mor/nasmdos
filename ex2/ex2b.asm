; ex2b. function that gets hex value, subtratcs 4 from it, and saves the sum in the accumulator AX.

ORG 0X100

; --------------------- equates, definitions --------------------------------

      
      exit_command EQU 0x4c00
      sub_val EQU 0x4                ; the function subtracts the constant value 4H from the parameter

      %define accumulator AX         ; using more meaningful names for buffers.
      %define dos_caller AX          ; AX has two rules
      %define DOS int 0x21
  

;--------------------- macros ------------------------------------------------

      %define sub_4(num) num-sub_val             ; single line macro that gets one hex value, ; subtracts 4 hex from it,
                                                 ;  and saves the resault in the accumulator (AX)
                         
;----------------- PROGRAM ----------------------------------------------------   
section .text
      xor AX,AX                               ; empty AX before summing to it.
      mov accumulator,sub_4(0xFA43)           ; call sub_4 macro.
      

END: mov dos_caller, exit_command    ; AX <- 0x4c00 = exit code for dos.
          DOS                        ; exit program.



